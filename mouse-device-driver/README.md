# mouse-device-driver
Simple mouse device driver that increases volume on right click and decreases volume on left click

Steps inserting Mouse Driver(needs root access) to your kernel:

1. mknod /dev/panaSPotta c 90 1 
 
2. chmod a+r+w /dev/panaSPotta

3. Go to the directory that contains the source code. 

4. make

5. insmod mouse.ko

6. gcc test.c -o test


Steps for running the Mouse Driver:
1. bash ./Script.sh

2. You will see following output:
    > ``` Do you want to control your volume with mouse??? [Y/n]```

3. Type: `Y` if you want your mouse to be a volume controller

    > ``` Do you want to control your volume with mouse??? [Y/n] Y ```

4. Try to click left on your mouse, then you will see following output:
    > ``` Decrease Volume, Current Volume is: X%```

5. Try to click right on your mouse, then you will see following output:
    > ``` Increase Volume, Current Volume is: X% ```
